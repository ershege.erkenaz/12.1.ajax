export function initializeCommunitySection() {
    const learnSection = document.getElementById('learn-section');
    const communitySection = document.createElement('div');
    const title = document.createElement('h1');
    const subTitle = document.createElement('p');
    const cards = document.createElement('div');

    communitySection.setAttribute('id', 'communitySection');
    title.setAttribute('id', 'community-title');
    subTitle.setAttribute('id', 'community-subTitle');
    cards.setAttribute('id', 'cards-div');



    fetch("http://localhost:3000/community", {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },
    })
    .then(response => {
        if (!response.ok) {
            throw new Error("Network response was not ok");
        }
        return response.json();
    })
    .then(data => {
        const communitySection = document.getElementById("cards-div");
        data.forEach(member => {
            const memberElement = document.createElement("div");
            memberElement.setAttribute('id','memberElement');
            memberElement.classList.add("member");
            memberElement.innerHTML = `
                <div class="avatar">
                    <img src="${member.avatar}" alt="Avatar">
                </div>
                <div class="details">
                    <p id="lorem-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
                    <h3>${member.firstName} ${member.lastName}</h3>
                    <p>${member.position}</p>
                </div>
            `;
            cards.appendChild(memberElement);
        });
    })
    .catch(error => {
        console.error("Error occurred while fetching community data:", error);
    });

    title.innerHTML = "Big Community of <br> People Like You";
    subTitle.innerHTML = "We’re proud of our products, and we’re really excited <br> when we get feedback from our users.";

    
    communitySection.appendChild(title);
    communitySection.appendChild(subTitle);
    communitySection.appendChild(cards);
    document.body.appendChild(communitySection);
    learnSection.parentNode.insertBefore(communitySection, learnSection);
}
