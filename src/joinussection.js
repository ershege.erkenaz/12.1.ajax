import validate from './email-validator';

const JoinUsSection = (function () { // eslint-disable-line wrap-iife
  function init(typeText, buttonText) {
    const footer = document.getElementsByClassName('app-footer')[0];
    const joinSection = document.createElement('div');
    const mainText = document.createElement('h1');
    const secondaryText = document.createElement('p');
    const emailForm = document.createElement('FORM');
    const emailInput = document.createElement('INPUT');
    const emailButton = document.createElement('BUTTON');

    joinSection.setAttribute('id', 'join');
    mainText.setAttribute('id', 'mainText');
    secondaryText.setAttribute('id', 'secondaryText');

    emailForm.setAttribute('id', 'myForm');
    emailInput.setAttribute('id', 'emailInput');
    emailInput.setAttribute('type', 'text');
    emailInput.setAttribute('placeholder', 'Email');
    emailButton.setAttribute('id', 'emailButton');

    emailButton.innerHTML = buttonText;
    mainText.innerHTML = typeText;
    secondaryText.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.';

    // Populate Task
    var storedEmail = localStorage.getItem('email'); // eslint-disable-line vars-on-top
    // if the stored email exists
    if (storedEmail) {
      emailInput.value = storedEmail;
    }

    emailForm.addEventListener('submit', function (e) { // eslint-disable-line prefer-arrow-callback

      e.preventDefault();
      const subscribed = localStorage.getItem('subscribed') === 'true';
      const email = emailInput.value.trim();

      if (subscribed) {

        // Disable the button and apply styling
        this.disabled = true;
        this.style.opacity = 0.5;

        //http://localhost:3000/unsubscribe

        fetch("http://localhost:3000/unsubscribe", {
            method: "POST"
        })
        .then(response => {
            if (!response.ok) {
                throw new Error("Unsubscribe request failed");
            }
            console.log("Unsubscribe successful");
        })
        .catch(error => {
            console.error("Error occurred while unsubscribing:", error.message);
        })
        .finally(() => {
          // Re-enable the button and restore styling
          this.disabled = false;
          this.style.opacity = 1;
        });

        unsubscribe();
      } else if (validate(email)) {

        // Disable the button and apply styling
        this.disabled = true;
        this.style.opacity = 0.5;

        // http://localhost:3000/subscribe Task
        
          fetch('http://localhost:3000/subscribe', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: email })
          })
          .then(response => {
              if (response.ok) {
                  alert('Subscription successful!');
              } else if (response.status === 422) {
                response.json().then(data => {
                    alert('Error subscribing: ' + data.error); // Display error message
                });
              } else {
                  throw new Error('Error subscribing');
              }
          })
          .catch(error => {
              alert('Error subscribing: ' + error.message); // Display error message
          })
          .finally(() => {
            // Re-enable the button and restore styling
            this.disabled = false;
            this.style.opacity = 1;
          });

        subscribe(email);
      } else {
        alert('Please enter a valid email address.');
      }
      

    }, false);


    function unsubscribe() {
      emailInput.style.display = 'inline';
      emailButton.innerText = 'SUBSCRIBE';
      emailButton.style.margin = '';
      emailInput.value = '';
      localStorage.removeItem('email');
      localStorage.setItem('subscribed', 'false');
      localStorage.clear();
    }

    function subscribe(email) {
      emailInput.style.display = 'none';
      emailButton.innerText = 'UNSUBSCRIBE';
      emailButton.style.margin = 'auto';
      localStorage.setItem('email', email);
      localStorage.setItem('subscribed', 'true');
    }
    function updateUI() {
      const subscribed = localStorage.getItem('subscribed') === 'true';
      if (subscribed) {
        emailInput.style.display = 'none';
        emailButton.innerText = 'Unsubscribe';
        emailButton.style.margin = 'auto';
        localStorage.setItem('subscribed', 'true');
      } else {
        emailInput.style.display = 'inline';
        emailButton.innerText = 'Subscribe';
        emailButton.style.margin = '';
        localStorage.setItem('subscribed', 'false');
      }
    }
    updateUI();
    joinSection.appendChild(mainText);
    joinSection.appendChild(secondaryText);
    joinSection.appendChild(emailForm);
    document.body.appendChild(joinSection);
    emailForm.appendChild(emailInput);
    emailForm.appendChild(emailButton);
    footer.parentNode.insertBefore(joinSection, footer);
  }

  return {
    init: init, // eslint-disable-line object-shorthand
  };
})();

export default JoinUsSection;
