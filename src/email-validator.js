var VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

const validate = function (email) { // eslint-disable-line func-names
  return VALID_EMAIL_ENDINGS.some((validEnd) => email.endsWith(validEnd));
};

export default validate;
