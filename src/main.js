import JoinUsSection from './joinussection';
import './styles/style.css';
import { initializeCommunitySection } from './community-section.js'; //when i need to add {}

document.addEventListener('DOMContentLoaded', function () { // eslint-disable-line prefer-arrow-callback
  class SectionCreator {
    create(type) {
      switch (type) {
      case 'standard':
        return JoinUsSection.init('Join Our Program', 'SUBSCRIBE');
      case 'advanced':
        return JoinUsSection.init('Join Our Advanced Program', 'SUBSCRIBE TO ADVANCED PROGRAM');
      default: return JoinUsSection.init('Join Our Program', 'SUBSCRIBE');
      }
    }

    remove() { // eslint-disable-line class-methods-use-this
      const el = document.getElementById('join');
      el.remove();
    }
  }
  const sectionFactory = new SectionCreator();
  sectionFactory.create('standard');

  initializeCommunitySection();
})
